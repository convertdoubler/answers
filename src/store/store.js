import Vue from 'vue';
import Vuex from 'vuex';

import * as getters from './getters'
import * as mutations from './mutations'
import * as actions from './actions'

import tests from './modules/tests/module';
import navbar from './modules/navbar';

Vue.use(Vuex);

export const store = new Vuex.Store({
    getters,
    mutations,
    actions,
    modules: {
        tests,
        navbar
    }
});
