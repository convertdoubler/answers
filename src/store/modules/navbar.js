const state = {
    searchText: '',
    disallowedCharactersRegExp: /[^a-zA-Zа-яА-Я0-9]/g
};

const getters = {
    searchText: state => {
        return state.searchText;
    },
    disallowedCharactersRegExp: state => {
        return state.disallowedCharactersRegExp;
    }
};

const mutations = {
    updateSearchText: (state, payload) => {
        state.searchText = payload;
    }
};

const actions = {
    updateSearchText: ({ commit }, payload) => {
        commit('updateSearchText', payload)
    }
};

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}
