import toiTest from './data/toi'
import ditTest from './data/dit'

const state = {
    currentTestKey: 'toiTest',
    tests: {
        toiTest,
        ditTest
    }
};

const getters = {
    currentTestKey: state => {
        return state.currentTestKey;
    },
    currentTest: state => {
        return state.tests[state.currentTestKey];
    },
    questions: state => {
        return state.tests[state.currentTestKey].questions;
    },
    tests: state => {
        return state.tests;
    }
};

const mutations = {
    changeCurrentTest: (state, payload) => {
        if (Object.keys(state.tests).includes(payload)) {
            state.currentTestKey = payload;
        }
    }
};

const actions = {
    changeCurrentTest: ({ commit }, payload) => {
        commit('changeCurrentTest', payload);
    }
};

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}
