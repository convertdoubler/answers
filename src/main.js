import './styles';
import './scripts';

import Vue from 'vue';
import App from './App.vue'
import { store } from './store/store';

import { VTooltip } from 'v-tooltip'
Vue.directive('tooltip', VTooltip);

new Vue({
    el: '#app',
    store,
    render: h => h(App)
});
